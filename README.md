## A BLANK PROJECT FOR WORK

### GENERAL INFO:
1. The toolkit: **Gulp 4**.
2. The preprocessors: **SASS**, **BABEL**.
3. Frameworks: **none**.
4. The JS library: **jQuery**.
5. Support: **ES6**.

### HOW TO USE IT:
1. Open a terminal and input: `npm install --save-dev`.
2. Run the task: `gulp`.
3. Make all changes in files of the folder: **src/**.
4. Use **https://realfavicongenerator.net/** to generate favicons and move them to the folder: **favicons/**.
5. Use **https://icomoon.io/** to generate a web font from svg icons.

#### - HTML:
- Edit a home page in the file: **src/index.html**.
- Use the **gulp-rigger** for the html files.
- The layout and the connections are in the files: **src/layouts/**.

### - SCSS:
- Make all changes in the files: **src/scss/**.
- Main variables are in the files: **src/scss/_main/**.
- Read comments in the files: **style.scss**, **_config.scss** and **_main-config.scss**.
- The all plugins should have been added to **src/scss/_libs/**.

### - JS:
- Make the general changes in the file: **src/js/main.js**.
- The all plugins should have been added to **src/js/_libs/**.

### - FONTS
- The all fonts should have been added to **src/fonts/**.

### - IMAGES
- The all images should have been added to **src/img/**.